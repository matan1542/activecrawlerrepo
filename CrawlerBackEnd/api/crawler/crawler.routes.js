const express = require('express')
const { getUrls} = require('./crawler.controller')
const router = express.Router()

// middleware that is specific to this router

router.get('/', getUrls)

module.exports = router