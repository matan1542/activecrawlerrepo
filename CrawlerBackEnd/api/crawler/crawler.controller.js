let psl = require('psl');
const crawlerService = require('./crawler.service')
async function getUrls(req, res) {
    const url = 'https://developer.mozilla.org'
    const hostName = psl.get(extractHostname(url));
    console.log('hostName',hostName)
    try {
        const links = await crawlerService.getLinks(url,hostName)
        res.status(200).send(links)
    } catch (err) {
        res.status(500).send({ err: 'Failed in crawler service' })
    }
}
function extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("//") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}

module.exports = {
    getUrls
}