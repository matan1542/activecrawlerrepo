
const logger = require('../../services/logger.service')
const axios = require('axios')
const cheerio = require('cheerio');
module.exports = {
    getLinks
}

async function getLinks(url, hostName, count) {
    count = 100;
    let obselete = []; // array of what was crawled already
    let htmlData = await axios.get(url)
    let $ = cheerio.load(htmlData.data)
    let urls = $("a");
    //nested urls count
    let depth = 3;
    let depthCount = 0;
    const links = await crawlLinks({ urls, obselete, url, hostName, count })
    return obselete
    async function crawlLinks({ urls, obselete, url, hostName, count }) {
        try {
            if (count === obselete.length || depth === depthCount) {
                return obselete
            }
            depthCount++;
            const hrefs = createLinksArray({ urls, obselete, url, hostName, count })
            obselete.push(...hrefs)
            const asyncHtmlData = await Promise.all(createAsyncArray(hrefs))
            console.log(asyncHtmlData)
            asyncHtmlData.forEach((htmlData) => {
                $ = cheerio.load(htmlData.data)
                urls = $("a");
                crawlLinks({ urls, obselete, url: htmlData.config.url, count, hostName })
            })
        } catch (err) {
            throw new Error(err)
        }
    }
}

function createAsyncArray(hrefs) {
    return hrefs.map(async (href) => {
        return await axios.get(href)
    })
}

function createLinksArray({ urls, obselete, url, hostName, count }) {
    const links = []
    Object.keys(urls).forEach((item) => {
        if (obselete.length + links.length === count) {
            return links
        }
        if (urls[item].type === 'tag') {
            let href = urls[item].attribs.href
            if (href[href.length - 1] === '/' && href.length !== 1) {
                href = href.substring(0, href.length - 1)
            }
            if (url[url.length - 1] === '/' && url.length !== 1) {
                url = url.substring(0, url.length - 1)
            }
            if (href && !obselete.includes(href) && !obselete.includes(`${url}${href}`)) {
                href = href.trim();
                if (href.includes(hostName)) {
                    links.push(href)
                } else if (href[0] === '/') {
                    links.push(`${url}${href}`)
                }
            }
        }
    })
    return links
}

