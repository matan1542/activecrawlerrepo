import React from 'react'
import CrawlerPreview from './CrawlerPreview'

export default function CrawlerList({hrefs}) {
    return (
        <div className="hrefs-list">
            {hrefs.map((href,idx)=>{
                return <CrawlerPreview key={idx} href={href} idx={idx}/>
            })}
        </div>
    )
}
