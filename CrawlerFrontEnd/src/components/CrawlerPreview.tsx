import React from 'react'

export default function CrawlerPreview({href,idx}) {
    return (
        <div className="href-preview">
            <a href={href}>Link number: {idx}</a>
        </div>
    )
}
