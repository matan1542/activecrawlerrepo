import {urlService} from '../../services/url.service'
export function loadUrls() { // Action Creator
    return async dispatch => {
        try {
            const urls = await urlService.query()
            const action = {
                type: 'SET_LINKS',
                urls
            }
            dispatch(action);
            return urls
        } catch (err) {
            //@ts-ignore 
            throw new Error('Couldn\'t load the urls  :', err)
        }
    }
}
