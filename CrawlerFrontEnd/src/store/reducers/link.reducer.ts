const initialState = {
    urls: null
};
export function linkReducer(state = initialState, action) {
    switch (action.type) {
        case 'SET_LINKS':
            return { ...state, urls: action.urls }
        default:
            return state
    }
}
