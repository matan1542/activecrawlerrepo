import './style/style.scss';
import { Header } from './components/Header';
import { Provider, useDispatch } from 'react-redux';
import { store } from './store/store';
import { ToastProvider } from 'react-toast-notifications';
import { useEffect } from 'react';
import Crawler from './pages/Crawler';
function App() {
 
  return (
    <div className="App">
      <Provider store={store}>
        <ToastProvider>
          <Header />
          <main>
            <Crawler/>
          </main>
        </ToastProvider >
      </Provider>
    </div>
  );
}

export default App;
