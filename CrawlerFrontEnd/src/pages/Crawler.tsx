import React, { useRef } from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CrawlerList from '../components/CrawlerList'
import { loadUrls } from '../store/actions/link.actions'
import { useAppSelector } from '../store/hooks'

export default function Crawler() {
    const dispatch = useDispatch()
    const {urls} = useAppSelector((state) => state.linkModule)
    const isMounted = useRef(false)
    useEffect(() => {
        console.log('urls',urls)
        if(!isMounted.current){
            isMounted.current = true
            dispatch(loadUrls())
        }
    }, [])
    if(!urls) return <div>Loading...</div>
    return (
        <div className="crawler">
            <CrawlerList hrefs={urls}/>
        </div>
    )
}
