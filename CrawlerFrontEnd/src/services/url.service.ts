import Crawler from "crawler"
import {httpService} from './http.service'
export const urlService = {
    query
}
async function query() {
    const res = await httpService.get('crawler')
    return res
}
