import Axios from 'axios'

const BASE_URL = process.env.NODE_ENV === 'production'
    ? '/api/'
    : '//localhost:3030/api/'


var axios = Axios.create({
    withCredentials: true
})

export const httpService = {
    get: async (endpoint) => {
        console.log('BASE_URLendpoint',`${BASE_URL}${endpoint}`)
        const res = await axios.get(`${BASE_URL}${endpoint}`)
        return res.data
    }
}

